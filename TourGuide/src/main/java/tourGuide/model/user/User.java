package tourGuide.model.user;

import gpsUtil.location.VisitedLocation;
import tripPricer.Provider;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class User {

    public List<UserReward> userRewards = new ArrayList<>();
    private UUID userId;
    private String userName;
    private UserPreferences userPreferences = new UserPreferences();
    private Date latestLocationTimestamp;
    private List<VisitedLocation> visitedLocations = new ArrayList<>();
    private List<Provider> tripDeals = new ArrayList<>();

    public User() {
    }

    public User(UUID userId, String userName, String phoneNumber,
                String emailAddress) {
        this.userId = userId;
        this.userName = userName;
    }

    public UUID getUserId() {
        return this.userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void addToVisitedLocations(VisitedLocation visitedLocation) {
        visitedLocations.add(visitedLocation);
    }

    public List<VisitedLocation> getVisitedLocations() {
        return visitedLocations;
    }

    public void setVisitedLocations(List<VisitedLocation> visitedLocations) {
        this.visitedLocations = visitedLocations;
    }

    public void clearVisitedLocations() {
        visitedLocations.clear();
    }

    public void addUserReward(UserReward userReward) {
        if (userRewards.stream().noneMatch(r -> r.attraction.attractionName
                .equals(userReward.attraction.attractionName))) {
            userRewards.add(userReward);
        }
    }

    public List<UserReward> getUserRewards() {
        return userRewards;
    }

    public UserPreferences getUserPreferences() {
        return userPreferences;
    }

    public void setUserPreferences(final UserPreferences userPreferences) {
        this.userPreferences = userPreferences;
    }

    public VisitedLocation getLastVisitedLocation() {
        return visitedLocations.get(visitedLocations.size() - 1);
    }

    public void setTripDeals(List<Provider> tripDeals) {

        this.tripDeals = tripDeals;
    }

}

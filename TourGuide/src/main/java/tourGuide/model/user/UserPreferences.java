package tourGuide.model.user;

import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;
import javax.money.Monetary;


public class UserPreferences {

    private CurrencyUnit currency = Monetary.getCurrency("USD");
    private Money lowerPricePoint = Money.of(0, this.currency);
    private Money highPricePoint = Money.of(Integer.MAX_VALUE, this.currency);
    private int attractionProximity = Integer.MAX_VALUE;
    private int tripDuration = 1;
    private int ticketQuantity = 1;
    private int numberOfAdults = 1;
    private int numberOfChildren = 0;

    public UserPreferences() {
    }

    public int getTripDuration() {
        return this.tripDuration;
    }

    public void setTripDuration(final int tripDuration) {
        this.tripDuration = tripDuration;
    }

    public int getNumberOfAdults() {
        return this.numberOfAdults;
    }

    public void setNumberOfAdults(final int numberOfAdults) {
        this.numberOfAdults = numberOfAdults;
    }

    public int getNumberOfChildren() {
        return this.numberOfChildren;
    }

    public void setNumberOfChildren(final int numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

}

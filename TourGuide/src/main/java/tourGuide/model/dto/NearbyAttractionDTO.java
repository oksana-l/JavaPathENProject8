package tourGuide.model.dto;

import gpsUtil.location.Location;

public class NearbyAttractionDTO implements Comparable<NearbyAttractionDTO> {

    private String nameAttraction;
    private Location userLocation;
    private Location attractionLocation;
    private int distance;
    private Integer rewardPoints;

    public NearbyAttractionDTO() {

    }

    public String getNameAttraction() {
        return this.nameAttraction;
    }

    public void setNameAttraction(final String nameAttraction) {
        this.nameAttraction = nameAttraction;
    }

    public Location getUserLocation() {
        return this.userLocation;
    }

    public void setUserLocation(final Location userLocation) {
        this.userLocation = userLocation;
    }

    public Location getAttractionLocation() {
        return this.attractionLocation;
    }

    public void setAttractionLocation(final Location attractionLocation) {
        this.attractionLocation = attractionLocation;
    }

    public int getDistance() {
        return this.distance;
    }

    public void setDistance(final int distance) {
        this.distance = distance;
    }

    public Integer getRewardPoints() {
        return this.rewardPoints;
    }

    public void setRewardPoints(final Integer rewardPoints) {
        this.rewardPoints = rewardPoints;
    }

    /*
     * Comparator pour le tri des attractions par distance
     */
    @Override
    public int compareTo(final NearbyAttractionDTO d) {
        return distance - d.distance;
    }
}

package tourGuide.helper;

public enum InternalTestHelper {
    ;

    // Set this default up to 100,000 for testing
    private static int internalUserNumber = 100;

    public static int getInternalUserNumber() {
        return InternalTestHelper.internalUserNumber;
    }

    public static void setInternalUserNumber(final int internalUserNumber) {
        InternalTestHelper.internalUserNumber = internalUserNumber;
    }
}

package tourGuide.service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.dto.NearbyAttractionDTO;
import tourGuide.model.user.User;
import tourGuide.model.user.UserReward;
import tripPricer.Provider;
import tripPricer.TripPricer;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class TourGuideService {
    /**********************************************************************************
     s* <p>
     * Methods Below: For Internal Testing
     *
     **********************************************************************************/
    private static final String tripPricerApiKey = "test-server-api-key";
    private final GpsUtil gpsUtil;
    private final RewardCentral rewardCentral;
    private final RewardsService rewardsService;
    private final Executor taskExecutor;
    boolean testMode = true;
    private TripPricer tripPricer;
    // Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory
    private Map<String, User> internalUserMap = new HashMap<>();
    private Logger logger = LoggerFactory.getLogger(TourGuideService.class);

    public TourGuideService(final GpsUtil gpsUtil, final RewardsService rewardsService, TripPricer tripPricer,
                            final RewardCentral rewardCentral, final Executor taskExecutor) {
        this.gpsUtil = gpsUtil;
        this.rewardCentral = rewardCentral;
        this.rewardsService = rewardsService;
        this.taskExecutor = taskExecutor;
        this.tripPricer = tripPricer;

        if (this.testMode) {
            this.logger.info("TestMode enabled");
            this.logger.debug("Initializing users");
            this.initializeInternalUsers();
            this.logger.debug("Finished initializing users");
        }
    }

    public List<UserReward> getUserRewards(final User user) {
        return user.getUserRewards();
    }

    public VisitedLocation getUserLocation(User user) {
        if (user.getVisitedLocations().size() > 0) {
            user.getLastVisitedLocation();
        } else {
            trackUserLocation(user);
        }
        return user.getLastVisitedLocation();
    }

    public User getUser(String userName) {
        return internalUserMap.get(userName);
    }

    public List<User> getAllUsers() {
        return new ArrayList<>(internalUserMap.values());
    }

    public Map<String, Location> getAllUsersDTO() {
        Map<String, Location> locationByUserIdMap = new HashMap<>();
        for (User user : getAllUsers()) {
            locationByUserIdMap.put(user.getUserId().toString(), user.getLastVisitedLocation().location);
        }
        return locationByUserIdMap;
    }

    public void addUser(final User user) {
        if (!internalUserMap.containsKey(user.getUserName())) {
            internalUserMap.put(user.getUserName(), user);
        }
    }

    public List<Provider> getTripDeals(User user) {
        int cumulativeRewardPoints = user.getUserRewards().parallelStream()
                .mapToInt(UserReward::getRewardPoints).sum();
        CompletableFuture<List<Provider>> futureProvider = CompletableFuture.supplyAsync(() ->
                tripPricer.getPrice(TourGuideService.tripPricerApiKey, user.getUserId(), user.getUserPreferences().getNumberOfAdults()
                        , user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(),
                        cumulativeRewardPoints));
        futureProvider.thenApply(providers -> {
            user.setTripDeals(providers);

            logger.debug("getTripDeals : " + user.getUserName());
            for (int i = 0; i < providers.size(); i++) {
                logger.debug("providerName : " + providers.get(i).name + ", price : " + providers.get(i).price);
            }
            logger.debug("cumulativeRewardPoints : " + cumulativeRewardPoints);
            return providers;
        });

        return futureProvider.join();
    }

    public CompletableFuture<Void> trackUserLocation(User user) {
        Locale.setDefault(Locale.US);
        return CompletableFuture.supplyAsync(() -> {
            VisitedLocation visitedLocation = gpsUtil.getUserLocation(user.getUserId());
            user.addToVisitedLocations(visitedLocation);
            return visitedLocation;
        }, taskExecutor).thenAccept(location -> {
            rewardsService.calculateRewards(user);
        }).exceptionally(throwable -> {
            System.out.println("ERROR : " + throwable.getMessage());
            return null;
        });
    }

    public List<NearbyAttractionDTO> getNearbyAttractions(String userName) {
        Location userLocation = getUserLocation(getUser(userName)).location;

        return gpsUtil.getAttractions().stream()
                .map(attraction -> {
                    NearbyAttractionDTO newAttraction = new NearbyAttractionDTO();
                    Location attractionLocation = new Location(attraction.longitude, attraction.latitude);

                    newAttraction.setUserLocation(userLocation);
                    newAttraction.setNameAttraction(attraction.attractionName);
                    newAttraction.setAttractionLocation(attractionLocation);
                    newAttraction.setDistance((int) rewardsService.getDistance(
                            userLocation, attractionLocation));
                    newAttraction.setRewardPoints(rewardCentral.getAttractionRewardPoints(
                            getUser(userName).getUserId(), attraction.attractionId));

                    return newAttraction;
                })
                .sorted(Comparator.comparing(NearbyAttractionDTO::getDistance))
                .limit(5)
                .collect(Collectors.toList());
    }

    private void initializeInternalUsers() {
        IntStream.range(0, InternalTestHelper.getInternalUserNumber()).parallel().forEach(i -> {
            String userName = "internalUser" + i;
            String phone = "000";
            String email = userName + "@tourGuide.com";
            User user = new User(UUID.randomUUID(), userName, phone, email);
            generateUserLocationHistory(user);
            internalUserMap.put(userName, user);
        });
        logger.debug("Created {} internal test users.", InternalTestHelper.getInternalUserNumber());

    }

    private void generateUserLocationHistory(User user) {
        IntStream.range(0, 3).forEach(i ->
                user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(
                        generateRandomLatitude(), generateRandomLongitude()
                ), getRandomTime())));
    }

    private double generateRandomLongitude() {
        final double leftLimit = -180;
        final double rightLimit = 180;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    private double generateRandomLatitude() {
        final double leftLimit = -85.05112878;
        final double rightLimit = 85.05112878;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    private Date getRandomTime() {
        final LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
        return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
    }
}

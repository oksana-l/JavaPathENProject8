package tourGuide.service.tracker;

import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tourGuide.model.user.User;
import tourGuide.service.TourGuideService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Service
public class Tracker extends Thread {
    private static final long trackingPollingInterval = TimeUnit.MINUTES.toSeconds(5);
    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private TourGuideService tourGuideService;
    private Logger logger = LoggerFactory.getLogger(Tracker.class);
    private boolean stop;

    public Tracker(TourGuideService tourGuideService) {
        this.tourGuideService = tourGuideService;

        this.executorService.submit(this);
        this.addShutDownHook();
    }

    /**
     * Assures to shut down the Tracker thread
     */
    private void addShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            this.stop = true;
            executorService.shutdownNow();
        }));
    }

    @Override
    public void run() {
        StopWatch stopWatch = new StopWatch();
        while (true) {
            if (Thread.currentThread().isInterrupted() || this.stop) {
                logger.debug("Tracker stopping");
                break;
            }

            List<User> allUsers = tourGuideService.getAllUsers();
            List<CompletableFuture<Void>> tasksFuture = new ArrayList<>();
            logger.debug("Begin Tracker. Tracking {} users.", allUsers.size());
            stopWatch.start();
            allUsers.parallelStream().forEach(user -> tasksFuture.add(tourGuideService.trackUserLocation(user)));

            try {
                CompletableFuture.allOf(tasksFuture.toArray(new CompletableFuture[0])).get();
            } catch (Exception e) {
                logger.error("Error when waiting trackerUserLocation", e);
            }

            stopWatch.stop();
            logger.debug("Tracker Time Elapsed: {} seconds.", TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
            stopWatch.reset();
            try {
                logger.debug("Tracker sleeping");
                TimeUnit.SECONDS.sleep(Tracker.trackingPollingInterval);
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}

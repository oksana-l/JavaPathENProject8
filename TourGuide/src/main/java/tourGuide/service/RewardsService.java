package tourGuide.service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import rewardCentral.RewardCentral;
import tourGuide.model.user.User;
import tourGuide.model.user.UserReward;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

@Service
public class RewardsService {
    private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;
    Logger logger = LoggerFactory.getLogger(RewardsService.class);
    private GpsUtil gpsUtil;
    private RewardCentral rewardsCentral;
    private Executor taskExecutor;
    // proximity in miles
    private int attractionProximityRange;
    private int proximityBuffer;

    public RewardsService(GpsUtil gpsUtil, RewardCentral rewardCentral, Executor taskExecutor,
                          @Value("10") int proximityBuffer, @Value("20000") int attractionProximityRange) {
        this.gpsUtil = gpsUtil;
        this.rewardsCentral = rewardCentral;
        this.taskExecutor = taskExecutor;
        this.proximityBuffer = proximityBuffer;
        this.attractionProximityRange = attractionProximityRange;
    }

    /**
     * Calculate the reward for a user
     *
     * @return
     */
    public CompletableFuture<Void> calculateRewards(User user) {
        List<VisitedLocation> userLocations = user.getVisitedLocations();
        return CompletableFuture.supplyAsync(() -> gpsUtil.getAttractions(), taskExecutor)
                .thenAcceptAsync(attractions -> {
                    userLocations.forEach(visitedLocation -> {
                        attractions.forEach(attraction -> {
                            if (user.getUserRewards().stream().noneMatch(r -> r.attraction.attractionName
                                    .equals(attraction.attractionName))) {
                                if (nearAttraction(visitedLocation, attraction)) {
                                    getRewardPoints(attraction, user).thenAcceptAsync(points ->
                                            user.addUserReward(new UserReward(visitedLocation, attraction, points)), taskExecutor);
                                }
                            }
                        });
                    });
                }, taskExecutor);
    }


    public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
        return getDistance(attraction, location) <= attractionProximityRange;
    }

    private boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {
        return getDistance(attraction, visitedLocation.location) <= proximityBuffer;
    }

    private CompletableFuture<Integer> getRewardPoints(Attraction attraction, User user) {
        return CompletableFuture.supplyAsync(() -> this.rewardsCentral.getAttractionRewardPoints(
                attraction.attractionId, user.getUserId()), taskExecutor);
    }

    public double getDistance(final Location loc1, final Location loc2) {
        final double lat1 = Math.toRadians(loc1.latitude);
        final double lon1 = Math.toRadians(loc1.longitude);
        final double lat2 = Math.toRadians(loc2.latitude);
        final double lon2 = Math.toRadians(loc2.longitude);

        final double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
                + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

        final double nauticalMiles = 60 * Math.toDegrees(angle);
        return RewardsService.STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
    }

}

package tourGuide.configuration;

import gpsUtil.GpsUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import rewardCentral.RewardCentral;
import tripPricer.TripPricer;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Configuration
@EnableAsync
public class TourGuideModule {

    @Bean
    public GpsUtil getGpsUtil() {
        return new GpsUtil();
    }

    @Bean
    public RewardCentral getRewardCentral() {
        return new RewardCentral();
    }

    @Bean
    public Executor getTaskExecutor() {
        return Executors.newFixedThreadPool(1000);
    }

    @Bean
    public TripPricer getTripPricer() {
        return new TripPricer();
    }
}

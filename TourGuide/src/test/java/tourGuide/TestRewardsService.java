package tourGuide;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.Before;
import org.junit.Test;
import rewardCentral.RewardCentral;
import tourGuide.model.user.User;
import tourGuide.model.user.UserReward;
import tourGuide.service.RewardsService;

import java.util.Arrays;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestRewardsService {

    private GpsUtil gpsUtil;
    private RewardCentral rewardCentral;
    private RewardsService rewardsService;
    private ThreadPoolExecutor taskExecutor;
    private Attraction attraction;

    @Before
    public void setUp() {
        gpsUtil = mock(GpsUtil.class);
        rewardCentral = mock(RewardCentral.class);
        taskExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);
        rewardsService = new RewardsService(gpsUtil, rewardCentral, taskExecutor, 10, 2);

        attraction = new Attraction("Disneyland", "Anaheim", "Texas", 24, 110);
    }


    @Test
    public void isWithinAttractionProximity() {
        Location location = new Location(24.001, 110);
        Attraction farAttraction = new Attraction("Test", "Anaheim", "Texas", -80, -91);
        assertTrue(rewardsService.isWithinAttractionProximity(attraction, location));
        assertFalse(rewardsService.isWithinAttractionProximity(farAttraction, location));
    }

    @Test
    public void shouldCalculateRewardsTest() {

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        Attraction farAttraction = new Attraction("Test", "Anaheim", "Texas", -80, -91);
        when(gpsUtil.getAttractions()).thenReturn(Arrays.asList(attraction, farAttraction));
        when(rewardCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId())).thenReturn(5460);

        user.getVisitedLocations().add(
                new VisitedLocation(
                        user.getUserId(), new Location(24, 110), new Date())
        );
        user.getVisitedLocations().add(
                new VisitedLocation(
                        user.getUserId(), new Location(2, 3), new Date())
        );

        rewardsService.calculateRewards(user);

        waitAsyncTasks();

        assertEquals(1, user.getUserRewards().size());
        assertEquals(5460, user.getUserRewards().get(0).getRewardPoints());
        assertEquals(attraction.attractionId, user.getUserRewards().get(0).attraction.attractionId);
    }

    private void waitAsyncTasks() {
        while (taskExecutor.getActiveCount() > 0) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void addUserReward() {
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        Attraction attraction = new Attraction("Disneyland", "Anaheim", "Texas", 100, 200);
        user.addUserReward(new UserReward(null, attraction, 234));
        assertEquals(1, user.getUserRewards().size());

        user.addUserReward(new UserReward(null, attraction, 567));
        assertEquals(1, user.getUserRewards().size());
        assertEquals(234, user.getUserRewards().get(0).getRewardPoints());
    }
}

package tourGuide;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.Before;
import org.junit.Test;
import rewardCentral.RewardCentral;
import tourGuide.configuration.TourGuideModule;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.user.User;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tripPricer.TripPricer;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class TestPerformance {

    RewardCentral rewardCentral;
    /*
     * A note on performance improvements:
     *
     *     The number of users generated for the high volume tests can be easily adjusted via this method:
     *
     *     		InternalTestHelper.setInternalUserNumber(100000);
     *
     *
     *     These tests can be modified to suit new solutions, just as long as the performance metrics
     *     at the end of the tests remains consistent.
     *
     *     These are performance metrics that we are trying to hit:
     *
     *     highVolumeTrackLocation: 100,000 users within 15 minutes:
     *     		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
     *
     *     highVolumeGetRewards: 100,000 users within 20 minutes:
     *          assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
     */
    private TourGuideModule tourGuideModule;
    private GpsUtil gpsUtil;
    private TripPricer tripPricer;
    private RewardsService rewardsService;
    private TourGuideService tourGuideService;
    private Executor taskExecutor;
    private List<User> allUsers;
    private StopWatch stopWatch;

    @Before
    public void setUp() {
        Locale.setDefault(Locale.ENGLISH);
        tourGuideModule = new TourGuideModule();
        gpsUtil = tourGuideModule.getGpsUtil();
        tripPricer = tourGuideModule.getTripPricer();
        rewardCentral = tourGuideModule.getRewardCentral();
        taskExecutor = tourGuideModule.getTaskExecutor();
        rewardsService = new RewardsService(gpsUtil, rewardCentral, taskExecutor, 10, 20000);

        InternalTestHelper.setInternalUserNumber(100000);
        stopWatch = new StopWatch();
        tourGuideService = new TourGuideService(gpsUtil, rewardsService, tripPricer, rewardCentral, taskExecutor);
        allUsers = tourGuideService.getAllUsers();

    }

    @Test
    public void highVolumeTrackLocation() throws Exception {
        // Users should be incremented up to 100,000, and test finishes within 15 minutes
        stopWatch.start();

        allUsers.parallelStream().forEach(user -> tourGuideService.trackUserLocation(user));

        waitAsyncTasks();

        stopWatch.stop();

        System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS
                .toSeconds(stopWatch.getTime()) + " seconds.");
        assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS
                .toSeconds(stopWatch.getTime()));
    }

    private void waitAsyncTasks() {
        ThreadPoolExecutor executorService = (ThreadPoolExecutor) taskExecutor;
        while (executorService.getActiveCount() > 0) {
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void highVolumeGetRewards() {
        // Users should be incremented up to 100,000, and test finishes within 20 minutes
        Attraction attraction = gpsUtil.getAttractions().get(0);

        allUsers.parallelStream().forEach(u -> u.addToVisitedLocations(new VisitedLocation(
                u.getUserId(), attraction, new Date())));
        stopWatch.start();
        allUsers.parallelStream().forEach(user -> rewardsService.calculateRewards(user));

        waitAsyncTasks();
        stopWatch.stop();

        for (User user : allUsers) {
            assertTrue(user.getUserRewards().size() > 0);
        }

        System.out.println("highVolumeGetRewards: Time Elapsed: " + TimeUnit.MILLISECONDS
                .toSeconds(stopWatch.getTime()) + " seconds.");
        assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS
                .toSeconds(stopWatch.getTime()));
    }

}

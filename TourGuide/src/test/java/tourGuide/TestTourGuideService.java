package tourGuide;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.Before;
import org.junit.Test;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.model.dto.NearbyAttractionDTO;
import tourGuide.model.user.User;
import tourGuide.model.user.UserPreferences;
import tourGuide.model.user.UserReward;
import tourGuide.service.RewardsService;
import tourGuide.service.TourGuideService;
import tripPricer.Provider;
import tripPricer.TripPricer;

import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestTourGuideService {
    private GpsUtil gpsUtil;
    private Executor taskExecutor;
    private TourGuideService tourGuideService;
    private TripPricer tripPricer;
    private User user;
    private VisitedLocation visitedLocation;

    @Before
    public void setUp() {
        gpsUtil = mock(GpsUtil.class);
        tripPricer = mock(TripPricer.class);
        RewardCentral rewardCentral = mock(RewardCentral.class);
        taskExecutor = Executors.newFixedThreadPool(1);
        RewardsService rewardsService = new RewardsService(gpsUtil, rewardCentral, taskExecutor, 10, 20000);
        tourGuideService = new TourGuideService(gpsUtil, rewardsService, tripPricer, rewardCentral, taskExecutor);
        InternalTestHelper.setInternalUserNumber(0);
        UUID uuid = UUID.randomUUID();
        user = new User(uuid, "Jon", "000", "jon@tourGuide.com");
        Location location = new Location(100, 200);
        visitedLocation = new VisitedLocation(uuid, location, new Date());
    }

    //Test passed
    @Test
    public void shouldGetUserLocationTest() {
        user.addToVisitedLocations(visitedLocation);
        VisitedLocation testVisitedLocation = tourGuideService.getUserLocation(user);

        assertEquals(testVisitedLocation.userId, user.getUserId());
    }

    private void waitAsyncTasks() {
        ThreadPoolExecutor executor = (ThreadPoolExecutor) taskExecutor;
        while (0 < executor.getActiveCount()) {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    //Test passed
    @Test
    public void shouldAddUserTest() {
        User user2 = new User(UUID.randomUUID(), "Jon2", "000", "jon2@tourGuide.com");

        tourGuideService.addUser(user);
        tourGuideService.addUser(user2);

        User addedUser = tourGuideService.getUser(user.getUserName());
        User addedUser2 = tourGuideService.getUser(user2.getUserName());

        assertEquals(user, addedUser);
        assertEquals(user2, addedUser2);
    }

    //Test passed
    @Test
    public void shouldGetAllUsersTest() {
        User user2 = new User(UUID.randomUUID(), "Jon2", "000", "jon2@tourGuide.com");

        tourGuideService.addUser(user);
        tourGuideService.addUser(user2);

        List<User> allUsers = tourGuideService.getAllUsers();

        assertTrue(allUsers.contains(user));
        assertTrue(allUsers.contains(user2));
    }

    //Test passed
    @Test
    public void shouldTrackUserTest() {
        when(gpsUtil.getUserLocation(any())).thenReturn(visitedLocation);
        tourGuideService.trackUserLocation(user);
        waitAsyncTasks();
        assertEquals(1, user.getVisitedLocations().size());
        assertEquals(user.getUserId(), user.getLastVisitedLocation().userId);
    }

    //Test passed
    @Test
    public void shouldGetNearbyAttractionsTest() {
        Locale.setDefault(Locale.US);
        List<Attraction> attractionsList = Arrays.asList(
                new Attraction("Disneyland", "Anaheim", null, 10, 70),
                new Attraction("Legend Valley", "Thornville", null, 35, 120),
                new Attraction("McKinley Tower", "Anchorage", null, 60, 10),
                new Attraction("Flatiron Building", "New York City", null, -70, 40),
                new Attraction("Fallingwater", "Mill Run", null, -30, 100),
                new Attraction("Union Station", "Washington D.C.", null, 80, -150)
        );
        when(gpsUtil.getAttractions()).thenReturn(attractionsList);
        List<VisitedLocation> visitedLocationsList = new ArrayList<>();
        visitedLocationsList.add(visitedLocation);
        user.setVisitedLocations(visitedLocationsList);
        tourGuideService.addUser(user);

        List<NearbyAttractionDTO> attractionsDto = tourGuideService.getNearbyAttractions("Jon");
        assertEquals(5, attractionsDto.size());
    }

    @Test
    public void shouldGetTripDealsTest() {
        user.addUserReward(new UserReward(null, new Attraction("Disneyland", "", "", 0, 0), 10));
        user.addUserReward(new UserReward(null, new Attraction("Asterix", "", "", 0, 0), 5));
        UserPreferences userPreferences = new UserPreferences();
        userPreferences.setTripDuration(2);
        userPreferences.setNumberOfAdults(2);
        userPreferences.setNumberOfChildren(3);
        user.setUserPreferences(userPreferences);
        List<Provider> providers = Arrays.asList(
                new Provider(user.getUserId(), "Dancing Waves Cruselines and Partners", 400),
                new Provider(user.getUserId(), "Sunny Days", 90),
                new Provider(user.getUserId(), "Holiday Travels", 150),
                new Provider(user.getUserId(), "Live Free", 750),
                new Provider(user.getUserId(), "AdventureCo", 300)
        );
        when(tripPricer.getPrice("test-server-api-key", user.getUserId(), 2, 3, 2, 15))
                .thenReturn(providers);

        assertEquals(providers, tourGuideService.getTripDeals(user));
    }
}
